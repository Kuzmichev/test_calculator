#!/bin/bash

[[ ! -e /.dockerenv ]] && exit 0

set -xe

apt-get update -yqq
apt-get install git zip unzip -yqq

curl --location --output /usr/local/bin/composer https://getcomposer.org/composer.phar
chmod +x /usr/local/bin/composer

