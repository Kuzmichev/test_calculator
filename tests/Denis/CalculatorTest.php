<?php

namespace Denis\Tests;

use Denis\Calculator;
use Exception;

class CalculatorTest extends \PHPUnit\Framework\TestCase
{
    protected $calculator;

    protected function setUp()
    {
        $this->calculator = new Calculator();
    }

    public function testDoCorrect()
    {
        $expected = 6;
        $result = $this->calculator->calculate('2+2*2');
        $this->assertEquals($expected, $result);
    }

    public function testDoUncorrect()
    {
        $expected = 8;
        $result = $this->calculator->calculate('2+2*2');
        $this->assertNotEquals($expected, $result);
    }

    /**
     * @expectedException Exception
     */
    public function testDoMathException()
    {
        $this->calculator->calculate('0/2');
    }
    /**
     * @expectedException Exception
     */
    public function testDoSyntaxException()
    {
        $this->calculator->calculate('(1+1(+1');
    }

}
