<?php

namespace Denis;

use Exception;


/**
 * Class Calculator
 * @package Denis
 */
class Calculator
{
    const PATTERN_ALL_OPER = '/(?<sipmle>\((?:\-?\d+(?:\.?\d+)?[\+\-\*\/])+\-?\d+(?:\.?\d+)?\))/';
    const PATTERN_SIMLE_OPER = '/[\d\.]+[-\+\*\/][\d\.]+/';
    const PATTERN_MD = '/\(?(?<digit1>[\d.]+)(?<operation>[\*\/])(?<digit2>[\d.]+)\)?/';
    const PATTERN_PM = '/\(?(?<minus>[\-])?(?<digit1>[\d.]+)(?<operation>[\+\-])(?<digit2>[\d.]+)\)?/';
    const PATTERN_NOCORRECT = '/[^\d\*\/\-\+\-\.\(\)]/';
    const PATTERN_PAT = '/[\(\)]/';

    /**
     * @param $string
     * @param string $method
     * @return number
     * @throws Exception
     */
    public function calculate($string, $method = 'regexp')
    {
        if ($method == 'regexp') {
            return $this->calcRegExp($string);
        } elseif ($method == 'wolframalpha') {
            return $this->calcWolframAlpha($string);
        }else{
            throw new \Exception('Parameters error: Unknown method');
        }
    }

    /**
     * @param $string
     * @return number
     * @throws Exception
     */
    public function calcRegExp($string){
        $res = $string;
        if (preg_match(self::PATTERN_NOCORRECT, $res)){
            throw new \Exception('Syntax error: Invalid characters found');
        }
        if (substr_count($res, '(') != substr_count($res, ')')){
            throw new \Exception('Syntax error: No quotes');
        }
        while (preg_match(self::PATTERN_SIMLE_OPER, $res)) {
            if (preg_match(self::PATTERN_PAT, $res) && preg_match(self::PATTERN_ALL_OPER, $res)) {
                $callback = function ($m) {
                    return self::simpleOperation($m['sipmle']);
                };
                $res = preg_replace_callback(self::PATTERN_ALL_OPER,
                    $callback, $res, -1, $count
                );
            } else {
                if (preg_match(self::PATTERN_PAT, $res)){
                    throw new \Exception('Syntax error: Error in expression');
                }
                $res = self::simpleOperation($res);
            }
        }
        if ($res == $string){
            throw new \Exception('Syntax error: Error in expression');
        }

        return $res;
    }

    /**
     * @param $res
     * @return number
     * @throws Exception
     */
    public function simpleOperation($res)
    {
        // multiplication and division
        $count = 1;
        while ($count) {
            $res = preg_replace_callback(self::PATTERN_MD,
                function ($m) {
                    $res = 0;
                    if (!is_numeric($m['digit1']) || !is_numeric($m['digit2'])){
                        throw new \Exception('Syntax error: No figures found');
                    }
                    if ($m['operation'] == '*') {
                        $res = $m['digit1'] * $m['digit2'];
                    }
                    if (!$m['digit1'] || !$m['digit2']){
                        throw new \Exception('Arithmetic error: Division by zero');
                    }
                    if ($m['operation'] == '/') {
                        $res = $m['digit1'] / $m['digit2'];
                    }

                    return $res;
                }, $res, -1, $count
            );
        }
        // plus and minus
        $count = 1;
        while ($count) {
            $res = preg_replace_callback(self::PATTERN_PM,
                function ($m) {
                    if (!is_numeric($m['digit1']) || !is_numeric($m['digit2'])){
                        throw new \Exception('Syntax error: No figures found');
                    }
                    $res = 0;
                    $neg = 1;
                    if ($m['minus'] == '-') $neg = -1;
                    if ($m['operation'] == '-'){
                        $res =  $neg * $m['digit1'] - $m['digit2'];
                    }
                    if ($m['operation'] == '+'){
                        $res =  $neg * $m['digit1'] + $m['digit2'];
                    }
                    return $res;
                }, $res, -1, $count);
        }
        if (preg_match(self::PATTERN_SIMLE_OPER, $res)) {
            $res = self::simpleOperation($res);
        }

        return $res;
    }

    /**
     * @param $string
     * @return number
     * @throws Exception
     */
    public function calcWolframAlpha($string){
        $appId = getenv('WOLFRAMALPHA_APP_ID');
        if (empty($appId)){
            throw new \Exception('Parameter error: Not set wolfram alpha APP ID');
        }
        $engine = new \WolframAlpha\Engine($appId);
        $result = $engine->process($string);
        if ($result->success) {
            $xml = new \SimpleXMLElement($result->parsedXml->asXML());
            return $xml->pod[1]->subpod->plaintext->__toString();
        }else{
            throw new \Exception('Syntax error: Error in expression');
        }
    }

}
